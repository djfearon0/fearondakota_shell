/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fearondakotashell;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Dakota F
 */
public class FearonDakotaShell {

    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException{
        String commandLine;//Initialize command set
        BufferedReader console = new BufferedReader(new InputStreamReader(System.in));//Initialize reader for input
        
        ArrayList history = new ArrayList<String[]>();//History list of commands
        
        while(true){//Begin input loop
            System.out.print("jsh> ");//Line pointer
            commandLine = console.readLine();//Read input
            
            if(commandLine.equals(""))//Check if command is empty
                continue;//Continue
            
            if(commandLine.equals("exit"))//Check if command is exit
                System.exit(0);//End program
            
            history.add(commandLine);//Add line to history
            String[] command = commandLine.split(" ");//Split input into parameters
            
            if(commandLine.split("")[1].equals("!") && !(commandLine.split("")[2].equals("!"))){//Check if input is accessing history item
                String[] cmd = commandLine.split("");//Split ! and integer value
                int index = Integer.parseInt(cmd[2]);//Parse integer value for index
                String last = history.get(index).toString();//Retrieve command from history
                String[] l = last.split(" ");//Split command into parameters
                OSProcess.main(l);//Run retreived command
            }
            
            else if(command[0].equals("!!")){//Check if input is accessing previous command
                if(history.isEmpty()){//Check if history is empty
                    System.out.println("No history");//History is empty
                }
                else if(!history.isEmpty()){//History is not empty
                    String last = history.get(history.size()-2).toString();//Retreive the last command in history
                    String[] l = last.split(" ");//Split command into parameters
                    OSProcess.main(l);//Run retreived command
                } 
            }
            
            else if(command[0].toLowerCase().equals("history")){//Check if input is requesting input history
            for(int i = 0; i < history.size(); i++){//Iterate through history
                String string = history.get(i).toString();//Retrieve item from history
                String[] array = string.split(" ");//Split command into parameters
                System.out.print(i+": ");//Print index
                for(int j = 0; j < array.length; j++){//loop through retreived parameters
                    if(j<history.size()-1)//Check if parameters are not the last in history
                        System.out.print(array[j]+" ");//Print history item
                    else//Parameter is the last in history
                        System.out.print(array[i]);//Print final item
                }
                System.out.println();//Skip line
            }
            }
            
            
            
            else{
            try{
                OSProcess.main(command);//Create process based on parameters
            }catch(Exception ioe){//Catch if error
                System.out.println("Invalid command");//Command given is not a true command
            }
        }
        }
    }
}
