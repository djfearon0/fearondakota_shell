package fearondakotashell;
import java.io.*;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 *
 * @author Dakota F
 */
public class OSProcess {
    
    static File home = new File(System.getProperty("user.home"));//Current Working Directory
    static boolean valid;//Boolean for checking validity of commands
    
    public static void main (String [] args) throws IOException{//Process Builder Main
        if(args.length < 1){//If there is an empty command
            System.err.println("Usage:  java Process <command>");//Command error
        }
        
        valid = true;//Set validity to true
        
        if(args[0].toLowerCase().equals("history") || args[0].split("").equals("!")){//Check if args are special command
            valid = false;//Set valid to false to prevent false error message
        }
        
        if(valid == true){//Valid command accepted
        ProcessBuilder pb = new ProcessBuilder(args);//Create new process builder
        pb.directory(home);//Set process builder to the current working directory
        Process process = pb.start();//Start process builder
        
        
        InputStream is = process.getInputStream();//Create input
        InputStreamReader isr = new InputStreamReader(is);//Read input
        BufferedReader br = new BufferedReader(isr);//Read input
        
        if(args.length == 1 && args[0].equals("cd")){//Check if input is changing directories to the home directory
            pb.directory(new File(System.getProperty("user.home")));//Set the directory to the home directory
            home = new File(System.getProperty("user.home"));//Set current working directory to home
            System.out.println("Directory: " + pb.directory());//Print out current working directory
        }
        else if(args.length > 1 && args[0].equals("cd")){//Check if input is changing directories
            File myFile = new File(args[1]);//Create file object based on requested directory
                Path path = FileSystems.getDefault().getPath(""+home+"/"+myFile);//Specify file path to the directory
                if(Files.exists(path)){//Check if the path and directory exist
                    File newDir = new File(""+pb.directory() + "/"+myFile);//Path exists, create new directory path
                    home = newDir;//Change current working directory to the new directory
                    pb.directory(newDir);//Change to the new directory
                    System.out.println("Directory: " + pb.directory());//Print out new directory
                    pb.start();//Start process builder
                }
                else{System.out.printf("Directory '%s' does not exist%n", ""+args[1]);}//Specified directory does not exist
            }
        
        
        
                
        String line;//String object line
        while((line = br.readLine()) != null)//Check if line is empty
            System.out.println(line);//Print output from process
        
        br.close();//Close reader
    }
    }
}